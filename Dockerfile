FROM ibm-semeru-runtimes:open-17-jre
COPY target/image-3.1.0.jar image-3.1.0.jar
ENTRYPOINT ["java","-jar","/image-3.1.0.jar"]