package info.earty.image.presentation;

import info.earty.image.application.CreateCommand;
import info.earty.image.application.ImageCommandService;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ImageCommandApiService implements ImageCommandApi {

    private final ImageCommandService imageCommandService;

    @Override
    public void create(Attachment attachment, String workingCardId) {
        CreateCommand aCommand = new CreateCommand();
        aCommand.setWorkingCardId(workingCardId);
        aCommand.setFilename(attachment.getContentDisposition().getFilename());
        aCommand.setContentType(attachment.getContentType().toString());

        try {
            aCommand.setInputStream(attachment.getDataHandler().getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        imageCommandService.create(aCommand);
    }

}
