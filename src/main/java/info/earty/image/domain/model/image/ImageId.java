package info.earty.image.domain.model.image;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ImageId {
    String id;

    public static ImageId create(String imageId) {
        Assert.notNull(imageId, ImageId.class.getSimpleName() + " : image id cannot be null");
        return new ImageId(imageId);
    }
}
