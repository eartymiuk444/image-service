package info.earty.image.domain.model.workingcard;

import info.earty.image.domain.model.image.ImageId;

public interface PublishedCardImageService {

    PublishedCardImage publishedCardImageFrom(WorkingCardId workingCardId, ImageId imageId);

}
