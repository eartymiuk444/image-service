package info.earty.image.domain.model.image;

public interface ImageIdentityService {
    ImageId generate();
}
