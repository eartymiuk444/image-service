package info.earty.image.domain.model.workingcard;

import info.earty.image.domain.model.image.ImageId;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PublishedCardImage {

    WorkingCardId workingCardId;
    ImageId imageId;

    public static PublishedCardImage create(String workingCardId, String imageId) {
        return new PublishedCardImage(WorkingCardId.create(workingCardId), ImageId.create(imageId));
    }

}
