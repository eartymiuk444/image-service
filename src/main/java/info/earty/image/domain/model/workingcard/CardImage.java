package info.earty.image.domain.model.workingcard;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class CardImage {

    WorkingCardId workingCardId;

    public static CardImage create(String workingCardId) {
        return new CardImage(WorkingCardId.create(workingCardId));
    }

}
