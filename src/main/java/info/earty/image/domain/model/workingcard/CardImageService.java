package info.earty.image.domain.model.workingcard;

public interface CardImageService {

    CardImage cardImageFrom(WorkingCardId workingCardId);

}
