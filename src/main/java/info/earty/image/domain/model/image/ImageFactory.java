package info.earty.image.domain.model.image;

import info.earty.image.domain.model.workingcard.WorkingCardId;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.InputStream;

@Component
public class ImageFactory {

    public Image create(ImageId imageId, WorkingCardId workingCardId, String fileName, String mediaType, InputStream inputStream) {
        Assert.notNull(imageId, this.getClass().getSimpleName() + "image id cannot be null");

        Image image = new Image(imageId);
        image.setWorkingCardId(workingCardId);
        image.setFilename(fileName);
        image.setContentType(mediaType);
        image.setInputStream(inputStream);
        image.setPublished(false);

        return image;
    }

}
