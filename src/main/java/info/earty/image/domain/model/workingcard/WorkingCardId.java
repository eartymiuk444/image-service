package info.earty.image.domain.model.workingcard;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

@Value
@EqualsAndHashCode(doNotUseGetters = true)
@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class WorkingCardId {
    String id;

    public static WorkingCardId create(String workingCardId) {
        Assert.notNull(workingCardId, WorkingCardId.class.getSimpleName() + " : working card id cannot be null");
        return new WorkingCardId(workingCardId);
    }
}
