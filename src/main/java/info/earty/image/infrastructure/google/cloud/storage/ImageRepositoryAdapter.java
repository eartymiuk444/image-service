package info.earty.image.infrastructure.google.cloud.storage;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import com.google.common.collect.Lists;
import info.earty.image.domain.model.image.Image;
import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.image.ImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ImageRepositoryAdapter implements ImageRepository {

    private static final String GOOGLE_APIS_AUTH_ENDPOINT = "https://www.googleapis.com/auth/cloud-platform";

    @Value("${info.earty.image.service.google-storage.json-key}")
    private String googleStorageJsonKey;

    @Value("${info.earty.image.service.google-storage.bucket-name}")
    private String googleStorageBucketName;

    private final BlobImageAdapter blobImageAdapter;

    @Override
    public void add(Image image) {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

            BlobId blobId = BlobId.of(googleStorageBucketName, image.id().id());

            Map<String, String> metadata = new HashMap<>();
            metadata.put("workingCardId", image.workingCardId().id());
            metadata.put("filename", image.filename());
            metadata.put("published", image.published() ? Boolean.TRUE.toString() : Boolean.FALSE.toString());

            BlobInfo blobInfo = BlobInfo.newBuilder(blobId)
                    .setContentType(image.contentType())
                    .setMetadata(metadata)
                    .build();

            storage.createFrom(blobInfo, image.inputStream());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Image> findById(ImageId imageId) {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
            BlobId blobId = BlobId.of(googleStorageBucketName, imageId.id());
            Blob blob = storage.get(blobId);

            return blob == null ? Optional.empty() : Optional.of(blobImageAdapter.reconstituteAggregate(blob));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove(ImageId imageId) {
        try {
            GoogleCredentials credentials = GoogleCredentials
                    .fromStream(new ByteArrayInputStream(googleStorageJsonKey.getBytes(StandardCharsets.UTF_8)))
                    .createScoped(Lists.newArrayList(GOOGLE_APIS_AUTH_ENDPOINT));
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
            BlobId blobId = BlobId.of(googleStorageBucketName, imageId.id());
            storage.delete(blobId);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
