package info.earty.image.infrastructure.google.cloud.storage;

import com.google.cloud.storage.Blob;
import info.earty.image.domain.model.image.Image;
import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.workingcard.WorkingCardId;
import info.earty.infrastructure.reflection.ReflectionHelpers;
import org.springframework.stereotype.Component;

import java.nio.channels.Channels;

import static info.earty.infrastructure.reflection.ReflectionHelpers.accessibleConstructor;
import static info.earty.infrastructure.reflection.ReflectionHelpers.setField;

//TODO EA 4/3/2022 - Consider using the builder/loader pattern instead of reflection
@Component
public class BlobImageAdapter {

    public Image reconstituteAggregate(Blob blob) {
        Image image = ReflectionHelpers.newInstance(accessibleConstructor(Image.class, ImageId.class), ImageId.create(blob.getBlobId().getName()));
        setField(image, "filename", blob.getMetadata().get("filename"));
        setField(image, "workingCardId", WorkingCardId.create(blob.getMetadata().get("workingCardId")));
        setField(image, "published", blob.getMetadata().get("published").equals(Boolean.TRUE.toString()));
        setField(image, "contentType", blob.getContentType());
        setField(image, "inputStream", Channels.newInputStream(blob.reader()));
        return image;
    }

}
