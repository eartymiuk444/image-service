package info.earty.image.infrastructure.jaxrs.cxf;

import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;
import info.earty.workingcard.presentation.*;
import lombok.RequiredArgsConstructor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class ContentClientConfiguration {

    @Value("${info.earty.image.working-card.service.host}")
    private String contentServiceHost;

    @Value("${info.earty.image.working-card.service.port}")
    private String contentServicePort;

    private final JacksonJsonProvider jacksonJsonProvider;

    @Bean
    public WorkingCardQueryApi workingCardQueryApi() {
        return clientApiHelper(WorkingCardQueryApi.class);
    }

    private <T> T clientApiHelper(Class<T> clazz) {
        JAXRSClientFactoryBean clientFactory = new JAXRSClientFactoryBean();
        clientFactory.setAddress(String.format("http://%s:%s/", contentServiceHost, contentServicePort));
        clientFactory.setProvider(jacksonJsonProvider);
        clientFactory.setServiceClass(clazz);
        return (T) clientFactory.create();
    }

}
