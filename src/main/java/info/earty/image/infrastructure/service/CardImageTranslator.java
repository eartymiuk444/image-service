package info.earty.image.infrastructure.service;

import info.earty.workingcard.presentation.data.DraftCardJsonDto;
import info.earty.workingcard.presentation.data.PublishedCardJsonDto;
import info.earty.image.domain.model.workingcard.CardImage;
import info.earty.image.domain.model.workingcard.PublishedCardImage;
import org.springframework.stereotype.Component;

@Component
public class CardImageTranslator {

    public CardImage toCardImageFromRepresentation(DraftCardJsonDto draftCardJsonDto) {
        return CardImage.create(draftCardJsonDto.getWorkingCardId());
    }


}
