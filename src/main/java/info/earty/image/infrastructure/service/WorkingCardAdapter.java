package info.earty.image.infrastructure.service;

import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.workingcard.CardImage;
import info.earty.image.domain.model.workingcard.PublishedCardImage;
import info.earty.image.domain.model.workingcard.WorkingCardId;

public interface WorkingCardAdapter {

    PublishedCardImage toPublishedCardImage(WorkingCardId workingCardId, ImageId imageId);
    CardImage toCardImage(WorkingCardId workingCardId);

}
