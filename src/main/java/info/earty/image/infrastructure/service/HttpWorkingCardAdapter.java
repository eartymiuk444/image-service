package info.earty.image.infrastructure.service;

import info.earty.workingcard.presentation.WorkingCardQueryApi;
import info.earty.workingcard.presentation.data.DraftCardJsonDto;
import info.earty.workingcard.presentation.data.PublishedCardJsonDto;
import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.workingcard.CardImage;
import info.earty.image.domain.model.workingcard.PublishedCardImage;
import info.earty.image.domain.model.workingcard.WorkingCardId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import jakarta.ws.rs.WebApplicationException;
import java.util.Optional;

@Component("imageWorkingCardAdapter")
@RequiredArgsConstructor
public class HttpWorkingCardAdapter implements WorkingCardAdapter {

    private final WorkingCardQueryApi workingCardQueryApi;
    private final PublishedCardImageTranslator publishedCardImageTranslator;
    private final CardImageTranslator cardImageTranslator;

    @Override
    public PublishedCardImage toPublishedCardImage(WorkingCardId workingCardId, ImageId imageId) {
        PublishedCardJsonDto publishedCardJsonDto;
        try {
            publishedCardJsonDto = workingCardQueryApi.getPublishedCard(workingCardId.id());
        } catch (WebApplicationException e) {
            //NOTE EA 2022-04-18 if we get back a client error then we know we tried to retrieve a published card that doesn't exist.
            if (e.getResponse().getStatus() >= 400 && e.getResponse().getStatus() < 500) {
                return null;
            }
            else {
                throw e;
            }
        }

        Optional<PublishedCardJsonDto.Image> optionalImage = publishedCardJsonDto.getImages().stream().filter(
                image -> image.getImageId().equals(imageId.id())).findFirst();

        return optionalImage.map(image -> publishedCardImageTranslator.toPublishedCardImageFromRepresentation(
                publishedCardJsonDto.getWorkingCardId(), image)).orElse(null);
    }

    @Override
    public CardImage toCardImage(WorkingCardId workingCardId) {
        DraftCardJsonDto draftCardJsonDto;
        try {
            draftCardJsonDto = workingCardQueryApi.getDraftCard(workingCardId.id());
        } catch (WebApplicationException e) {
            //NOTE EA 2022-04-18 if we get back a client error then we know we tried to retrieve a published card that doesn't exist.
            if (e.getResponse().getStatus() >= 400 && e.getResponse().getStatus() < 500) {
                return null;
            }
            else {
                throw e;
            }
        }

        return cardImageTranslator.toCardImageFromRepresentation(draftCardJsonDto);
    }
}
