package info.earty.image.infrastructure.service;

import info.earty.image.domain.model.workingcard.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TranslatingCardImageService implements CardImageService {

    private final WorkingCardAdapter workingCardAdapter;

    @Override
    public CardImage cardImageFrom(WorkingCardId workingCardId) {
        return workingCardAdapter.toCardImage(workingCardId);
    }
}
