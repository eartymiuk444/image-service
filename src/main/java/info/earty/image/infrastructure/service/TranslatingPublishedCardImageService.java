package info.earty.image.infrastructure.service;

import info.earty.workingcard.presentation.WorkingCardQueryApi;
import info.earty.workingcard.presentation.data.PublishedCardJsonDto;
import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.workingcard.PublishedCardImage;
import info.earty.image.domain.model.workingcard.PublishedCardImageService;
import info.earty.image.domain.model.workingcard.WorkingCardId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TranslatingPublishedCardImageService implements PublishedCardImageService {

    private final WorkingCardAdapter workingCardAdapter;

    @Override
    public PublishedCardImage publishedCardImageFrom(WorkingCardId workingCardId, ImageId imageId) {
        return workingCardAdapter.toPublishedCardImage(workingCardId, imageId);
    }
}
