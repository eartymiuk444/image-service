package info.earty.image.infrastructure.uuid;

import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.image.ImageIdentityService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDImageIdentityService implements ImageIdentityService {
    @Override
    public ImageId generate() {
        return ImageId.create(UUID.randomUUID().toString());
    }
}
