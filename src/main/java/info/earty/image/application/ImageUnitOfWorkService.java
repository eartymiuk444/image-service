package info.earty.image.application;

import info.earty.image.domain.model.image.Image;

public interface ImageUnitOfWorkService {

    void saveImageAsPublished(Image image);

}
