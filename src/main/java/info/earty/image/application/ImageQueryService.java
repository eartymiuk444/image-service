package info.earty.image.application;

import info.earty.image.domain.model.image.Image;
import info.earty.image.domain.model.image.ImageId;
import info.earty.image.domain.model.image.ImageRepository;
import info.earty.image.domain.model.workingcard.PublishedCardImageService;
import info.earty.image.domain.model.workingcard.WorkingCardId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ImageQueryService {

    private final ImageRepository imageRepository;
    private final PublishedCardImageService publishedCardImageService;

    public Image get(String imageId) {
        Optional<Image> optionalImage = imageRepository.findById(ImageId.create(imageId));
        return optionalImage.orElseThrow(() -> new IllegalArgumentException(
                this.getClass().getSimpleName() + " : no image found with id"));
    }

    public Image getPublished(String imageId, String workingCardId) {
        Optional<Image> optionalImage = imageRepository.findById(ImageId.create(imageId));
        if (optionalImage.isPresent()) {
            Image image = optionalImage.get();

            if (image.published() || publishedCardImageService.publishedCardImageFrom(
                    WorkingCardId.create(workingCardId), ImageId.create(imageId)) != null) {
                return image;
            }
        }

        throw new IllegalArgumentException(this.getClass().getSimpleName() + " : no published image found with id");
    }

}
