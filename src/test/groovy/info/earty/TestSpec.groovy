package info.earty

import spock.lang.Specification

class TestSpec extends Specification {

    def "testing the test framework"() {
        given: "an integer that equals 1"
        Integer one = Integer.valueOf(1)

        expect: "it equals 1"
        one == 1
    }
}
